package utils;

/**
 * Created by elitebook 2560p on 14/06/2017.
 */
public class Repartie {
    public String start;
    public String libelle;

    public Repartie(String start, String libelle) {
        this.start = start;
        this.libelle = libelle;
    }

    public Repartie() {
    }
}
