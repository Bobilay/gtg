package controllers;

import controllers.deadbolt.Unrestricted;

import models.Autorisation;
import models.User;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elitebook 2560p on 23/03/2017.
 */
public class Auth extends Controller {


    @Unrestricted
    public static void authenticate(@Required String username, @Required String password) {
        // checkAuthenticity();
        ArrayList<String> autorise = new ArrayList<String>();
        User user =  User.authenticate(username, password);
        if (user == null) {
            flash.error(Messages.get("auth.error"));
            flash.put("username", username);
            login();
           // Application.index();
        } else {
            flash.success(Messages.get("auth.welcome"));
            session.put(User.SESSION_FULLNAME_KEY, String.format("%s %s", user.getPrenom(), user.getNom()));
            session.put(User.SESSION_USERNAME_KEY, user.getLogin());
            List<Autorisation> autorisations = user.getAutorisations();
            if (autorisations.size()>0 && !autorisations.isEmpty()){
                for (Autorisation auto: autorisations){
                    autorise.add(auto.getCode());
                }
                session.put(User.SESSION_AUTORISATION_KEY, autorise);
            }
            Application.index();
           // login();
        }
    }


    @Unrestricted
    public static void login() {
        render();
    }


    @Unrestricted
    public static void logout() {
        Scope.Session.current().clear();
        session.current().clear();
        login();
    }


    @Unrestricted
    public static void reinitialise() {
        render();
    }
}
