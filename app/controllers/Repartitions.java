package controllers;


import models.*;
import play.i18n.Messages;
import play.mvc.Before;

import play.mvc.Controller;
import play.mvc.Util;

import javax.swing.plaf.nimbus.State;

import models.Travail;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import utils.Repartie;

import java.util.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.lang.System;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */

public class Repartitions extends AppController {

    @Before
    public static void renderArgs() {

        User user = getCurrentUser();
        System.out.println("" + user.getId());

        StateRepartion stateRepartion = StateRepartion.find("idUser is ?", user.getId()).first();
        renderArgs.put("stateRepartion", stateRepartion);

        List<Employe> equipes = Employe.equipes("TT", false);
        List<Employe> equipesRepartis = Employe.equipes("TT", true);

        List<TypeInstrument> types = TypeInstrument.findAll();
        for (TypeInstrument type : types) {
            renderArgs.put(type.getAbrev(), Instrument.find("actif.libelle is ? and isrepartie is ? and typeInstrument is ?",
                    "Oui", false, type).fetch());
        }
        //List<Instrument> instruments = Instrument.find("isrepartie is ?", false).fetch();
        List<Instrument> instrumentsRepartis = Instrument.find("actif.libelle is ? and isrepartie is ?",
                "Oui", true).fetch();

        List<Travail> travaux = Travail.find("isrepartie is ?", false).fetch();
        List<Travail> travails = new ArrayList<Travail>();
        for (Travail travail : travaux) {
            travail.setDead(travail.getDeadLine());
            if(travail.getStatutTravail().getCode() != 4){
                travails.add(travail);
            }

        }

        List<Travail> travauxReparties = Travail.find("isrepartie is ?", true).fetch();
        List<Travail> travailsReparties = new ArrayList<Travail>();
        for (Travail travailRepartie : travauxReparties) {
            travailRepartie.setDead(travailRepartie.getDeadLine());
            if(travailRepartie.getStatutTravail().getCode() != 4){
                travailsReparties.add(travailRepartie);
            }

        }
        Set<String> set = new HashSet<String>();
        set = Employes.mergeTwoList(Employes.getEmployeStart(), Instruments.getInstrumentStart());
        List<Repartie> reparties = new ArrayList<Repartie>();
        Repartie repartie;
        for (String temp : set) {
            System.out.println(temp);
            repartie = new Repartie();
            repartie.start = temp;
            repartie.libelle = Employes.getEmployeByStart(temp) + "-" + Instruments.getInstrumentByStart(temp);
            reparties.add(repartie);
        }
        renderArgs.put("repartiesList", getJson(reparties));


        // renderArgs.put("instruments", instruments);
        renderArgs.put("instrumentList", getJson(instrumentsRepartis));
        renderArgs.put("typeList", getJson(types));

        renderArgs.put("travails", travails);
        renderArgs.put("travailList", getJson(travailsReparties));

        renderArgs.put("equipes", equipes);
        renderArgs.put("equipeList", getJson(equipesRepartis));

    }


    @Util
    public static User getCurrentUser() {
        String username = session.get(User.SESSION_USERNAME_KEY);
        if (username != null) {
            return User.findByLogin(username);
        } else {
            return null;
        }
    }

    public static boolean getStateRepartionByUser(Long idUser) {
        StateRepartion stateRepartion = StateRepartion.find("idUser is ?", idUser).first();
        if (stateRepartion != null) {
            return stateRepartion.isState();
        } else {
            return false;
        }
    }

    public static void unlock() {
        User user = getCurrentUser();
        StateRepartion stateRepartion = StateRepartion.find("idUser is ?", user.getId()).first();
        if (stateRepartion != null) {
            stateRepartion.setState(false);
            stateRepartion.save();
            Application.index();
        } else {
            Application.index();
        }
    }

    public static boolean getStateRepartionByAuthor() {
        StateRepartion stateRepartion = StateRepartion.find("state is ?", true).first();
        if (stateRepartion != null) {
            return stateRepartion.isState();
        } else {
            return false;
        }
    }

    public static void index() {
        User user = getCurrentUser();
        if (getStateRepartionByAuthor()) {
            // deja occupe donc on verifie si celui qui l'occupe est celui qui est connecté
            if (getStateRepartionByUser(user.getId())) {
                java.lang.System.out.println("Repartition veroullée par moi meme");
                flash.put("success", "Repartition veroullée par moi meme");
                travail();
            } else {
                //celui qui l'occupe est different donc acces refuse
                java.lang.System.out.println("Repartition veroullée par autre que moi meme");
                flash.put("error", "Repartition veroullée");
                Application.index();
            }
        } else {
            //personne ne l'occupe
            java.lang.System.out.println("Repartition non  veroullée");
            StateRepartion stateRepartion = StateRepartion.find("idUser is ?", user.getId()).first();
            if (stateRepartion != null) {
                stateRepartion.setState(true);
                stateRepartion.save();
            } else {
                StateRepartion newStateRepartion = new StateRepartion(true, user.getId());
                newStateRepartion.save();
            }
            travail();
        }
    }


    public static void edit(Long id) {
//       System model = System.findById(id);
//        notFoundIfNull(model);
//        render(model);

    }

    public static void delete(Long id) {
       /*System model = System.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }*/
    }

    /*public static void update(System model) {
       checkAuthenticity();
        notFoundIfNull(model);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }*/

    public static void travail() {
        render();
    }

    public static void equipe() {
        render();
    }

    public static void instrument() {
        render();
    }

    public static void repartitTravail(long id, String start) {

        java.lang.System.out.println(id);
        Travail travail = Travail.findById(id);
        if (travail != null) {
            travail.setIsrepartie(true);
            java.lang.System.out.println(start);
            travail.setStart(start);
            travail.setStatutTravail(StatutTravail.findByCode(3));
            travail.save();

        } else {
            java.lang.System.out.println("oki");

        }
    }

    public static void repartitInstrument(long id, String start) {

        java.lang.System.out.println(id);
        Instrument instrument = Instrument.findById(id);
        if (instrument != null) {
            instrument.setIsrepartie(true);
            java.lang.System.out.println(start);
            instrument.setStart(start);
            instrument.save();

        } else {
            java.lang.System.out.println("oki");

        }
    }

    public static void repartitEquipe(long id, String start) {

        java.lang.System.out.println(id);
        Employe employe = Employe.findById(id);
        if (employe != null) {
            employe.setIsrepartie(true);
            java.lang.System.out.println(start);
            employe.setStart(start);
            employe.save();

        } else {
            java.lang.System.out.println("oki");

        }
    }


}
