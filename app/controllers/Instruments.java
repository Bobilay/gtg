package controllers;

import models.*;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Instruments extends Controller {

    @Before
    public static void renderArgs() {
        List<Instrument> instruments = Instrument.findAll();
        List<TypeInstrument> typeInstruments = TypeInstrument.findAll();
        List<Marque> marques = Marque.findAll();
        List<Fournisseur> fournisseurs = Fournisseur.findAll();
        List<Disponibilite> disponibles = Disponibilite.findAll();
        List<Actif> actifs = Actif.findAll();
        renderArgs.put("actifs", actifs);
        renderArgs.put("fournisseurs", fournisseurs);
        renderArgs.put("disponibles", disponibles);
        renderArgs.put("marques", marques);
        renderArgs.put("instruments", instruments);
        renderArgs.put("typeInstruments", typeInstruments);
    }

    public static void index(){
            render();
    }

    public static void edit(Long id) {
       Instrument model = Instrument.findById(id);
        notFoundIfNull(model);
        render(model);
    }

    public static String getInstrumentByStart(String start){

        StringBuilder sb = new StringBuilder();
        List<Instrument> instrumentList= Instrument.find("start is ? and isrepartie is ?",start,true).fetch();
        for (Instrument instrument: instrumentList){
            sb.append(instrument.getTypeInstrument().getAbrev());
            sb.append("/");
        }
        if (sb.length()>1){
            return sb.substring(0,sb.length()-1);
        }else {
            return sb.toString();
        }
    }


    public static List<String> getInstrumentStart(){
        ArrayList<String> startsList = new ArrayList<String>();
        List<Instrument> instrumentList =Instrument.find("isrepartie is ?",true).fetch();
        for (Instrument i: instrumentList){
            startsList.add(i.getStart());
        }
        //  startsList = (ArrayList<String>) JPA.em().createQuery("SELECT i.start FROM Instrument i").getResultList();
        return startsList;
    }
    public static void addRemarque(Long id,String remarque,String date_remarque) {
       Instrument model = Instrument.findById(id);
       Remarque remarque1 = new Remarque(model,remarque,date_remarque);
        notFoundIfNull(model);
        if (remarque1.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            remarque(model.id);
        }
        flash.put("error", Messages.get("model.update.error"));
        remarque(model.id);
    }
    public static void addEntretien(Long id,double cout,String date_entretien,String detail,Long idfrs) {
       Instrument model = Instrument.findById(id);
       Fournisseur frs = Fournisseur.findById(idfrs);
        notFoundIfNull(model);
        notFoundIfNull(frs);
        Entretien entretien = new Entretien(model,cout,date_entretien,detail,frs);
        if (entretien.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            entretien(model.id);
        }
        flash.put("error", Messages.get("model.update.error"));
        entretien(model.id);
    }

    public static void remarque(Long id) {
       Instrument model = Instrument.findById(id);
        notFoundIfNull(model);
        render(model);
    }
    public static void entretien(Long id) {
       Instrument model = Instrument.findById(id);
        notFoundIfNull(model);
        render(model);
    }

    public static void delete(Long id) {
       Instrument model = Instrument.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }
    public static void deleteRemarque(Long id) {
       Remarque model = Remarque.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }
    public static void deleteEntretien(Long id) {
       Remarque model = Remarque.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(Instrument model) {
       checkAuthenticity();
        notFoundIfNull(model);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
}
