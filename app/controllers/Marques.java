package controllers;

import models.Marque;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Marques extends Controller {

    @Before
    public static void renderArgs() {
        List<Marque> marques = Marque.findAll();
        renderArgs.put("marques", marques);

    }

    public static void index(){
            render();
    }

    public static void edit(Long id) {
       Marque model = Marque.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
       Marque model = Marque.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(Marque model) {
       checkAuthenticity();
        notFoundIfNull(model);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
}
