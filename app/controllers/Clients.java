package controllers;

import models.*;
import models.System;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Clients extends Controller {

    @Before
    public static void renderArgs() {
        List<Client> clients = Client.findAll();
        renderArgs.put("clients", clients);

        List<Ville> villes = Ville.findAll();
        renderArgs.put("villes", villes);

        List<Pays> pays = Pays.findAll();
        renderArgs.put("pays", pays);
    }

    public static void index() {
        render();
    }

    public static void edit(Long id) {
        Client model = Client.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
        Client model = Client.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(Client model) {
        checkAuthenticity();
        notFoundIfNull(model);
        model.setFullName();
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
}
