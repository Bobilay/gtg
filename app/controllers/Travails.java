package controllers;

import models.*;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.lang.System;
import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Travails extends Controller {

    private static int codeSys;

    @Before
    public static void renderArgs() {

        List<Travail> travails = Travail.findAll();
        renderArgs.put("travails", travails);

        List<User> users = User.findAll();
        renderArgs.put("users", users);

//        List<StatutTravail> statuts = StatutTravail.findAll();
//        renderArgs.put("statuts", statuts);

        List<Employe> employes = Employe.findAll();
        renderArgs.put("employes", employes);

        List<Client> clients = Client.findAll();
        renderArgs.put("clients", clients);

        List<TypeMandat> typeMandats = TypeMandat.findAll();
        renderArgs.put("typeMandats", typeMandats);

        List<Bureau> bureaus = Bureau.findAll();
        renderArgs.put("bureaus", bureaus);

        List<Dossier> dossiers = Dossier.findAll();
        renderArgs.put("dossiers", dossiers);

    }

    public static void index(int code) {
        codeSys = code;
//        System.out.println("CODE SYSTEM "+ code);
        List<TypeTravail> typeTravails = TypeTravail.findBySystem(code);
        renderArgs.put("typeTravails", typeTravails);

        List<Travail> travails = Travail.findBySystem(code);
        renderArgs.put("travails", travails);

        List<StatutTravail> statuts = StatutTravails.findBySystem(code);
        renderArgs.put("statuts", statuts);

        render(travails, typeTravails, code,statuts);
    }


    public static void system(int code) {
        index(code);
    }

    public static void edit(Long id) {
        Travail model = Travail.findById(id);
        notFoundIfNull(model);
        List<TypeTravail> typeTravails = TypeTravail.findBySystem(codeSys);
        List<StatutTravail> statuts = StatutTravails.findBySystem(codeSys);
        renderArgs.put("statuts", statuts);
        renderArgs.put("typeTravails", typeTravails);
        render(model,typeTravails,statuts);

    }

    public static void delete(Long id) {
        Travail model = Travail.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(Travail model) {
        checkAuthenticity();
        notFoundIfNull(model);
        User user = Repartitions.getCurrentUser();
        if (user !=null){
            model.setUser(user);
            System.out.println("user current"+model.getUser());
        }
        if (model.getStatutTravail()!=null){

        }else {
              if (codeSys==0){
                  StatutTravail statutTravail=StatutTravail.find("code is ?",7).first();
                  model.setStatutTravail(statutTravail);
              }else {
                  StatutTravail statutTravail=StatutTravail.find("code is ?",2).first();
                  model.setStatutTravail(statutTravail);
              }
        }

        System.out.println("user"+model.getUser());
        model.setIsrepartie(false);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index(codeSys);
        }
        flash.put("error", Messages.get("model.update.error"));
        index(codeSys);

    }
}
