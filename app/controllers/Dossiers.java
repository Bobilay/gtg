package controllers;

import models.Dossier;
import models.System;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Dossiers extends Controller {

    @Before
    public static void renderArgs() {
        List<Dossier> dossiers = Dossier.findAll();
        renderArgs.put("dossiers", dossiers);

    }

    public static void index(){
            render();
    }

    public static void edit(Long id) {
       System model = System.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
       Dossier model = Dossier.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(Dossier model) {
       checkAuthenticity();
        notFoundIfNull(model);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
}
