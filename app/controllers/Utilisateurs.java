package controllers;

import com.google.gson.Gson;
import models.*;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.lang.System;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Utilisateurs extends Controller {

    @Before
    public static void renderArgs() {
        List<User> users = User.findAll();
        renderArgs.put("users", users);
        List<Autorisation> autorisations = Autorisation.findAll();
        renderArgs.put("autorisations", autorisations);
        List<Fonction> fonctions = Fonction.findAll();
        renderArgs.put("fonctions", fonctions);
    }

    public static void index() {
        render();
    }

    public static void edit(Long id) {
        User model = User.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
        User model = User.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(User model,String access,String password) {
        notFoundIfNull(model);
        //System.out.println("auto"+access);
        System.out.println("actif"+model.isActif());
        model.setPassword(password);

        model.setDisplayName();
        System.out.println("User=====>"+new Gson().toJson(model));
        String [] auto =access.split(",");
        if (auto.length>0){
            model.setAutorisations(updateAutorisation(auto));
        }
        if (model.validateAndSave()) {
            System.out.println(password);
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
    private static List<Autorisation> updateAutorisation( String [] auto){
        List<Autorisation> autorisations =new ArrayList<>();
        for(String w:auto){
            if (!w.isEmpty() && w !=null){
            Autorisation autorisation = Autorisation.findById(Long.parseLong(w));
            if (autorisation !=null){
                autorisations.add(autorisation);
            }
        }
        }
       return autorisations;
    }
}
