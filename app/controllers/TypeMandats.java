package controllers;

import models.TypeMandat;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class TypeMandats extends Controller {

    @Before
    public static void renderArgs() {
        List<TypeMandat> typeMandats = TypeMandat.findAll();
        renderArgs.put("typeMandats", typeMandats);

    }

    public static void index(){
            render();
    }

    public static void edit(Long id) {
       TypeMandat model = TypeMandat.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
       TypeMandat model = TypeMandat.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(TypeMandat model) {
       checkAuthenticity();
        notFoundIfNull(model);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
}
