package controllers;

import models.StatutTravail;
import models.TypeTravail;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.util.List;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class StatutTravails extends Controller {

    @Before
    public static void renderArgs() {
        List<StatutTravail> statutTravails = StatutTravail.findAll();
        renderArgs.put("statutTravails", statutTravails);

    }

    public static void index(){
            render();
    }

    public static void edit(Long id) {
       StatutTravail model = StatutTravail.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
       StatutTravail model = StatutTravail.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void update(StatutTravail model) {
       checkAuthenticity();
        notFoundIfNull(model);
        if (model.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();
    }

    public static List<StatutTravail> findBySystem(int code){
        return StatutTravail.find("system.code is ?", code).fetch();
    }
}
