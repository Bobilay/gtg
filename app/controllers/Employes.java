package controllers;

import models.*;
import models.System;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by elitebook 2560p on 22/02/2017.
 */
public class Employes extends Controller {

    @Before
    public static void renderArgs() {
        List<Employe> employes = Employe.findAll();
        renderArgs.put("employes", employes);

        List<Sexe> sexes = Sexe.findAll();
        renderArgs.put("sexes", sexes);

        List<Diplome> diplomes = Diplome.findAll();
        renderArgs.put("diplomes", diplomes);

        List<Fonction> fonctions = Fonction.findAll();
        renderArgs.put("fonctions", fonctions);

        List<Ville> villes = Ville.findAll();
        renderArgs.put("villes", villes);

        List<Pays> pays = Pays.findAll();
        renderArgs.put("pays", pays);

        List<System> systems = System.findAll();
        renderArgs.put("systems", systems);
    }

    public static String getEmployeByStart(String start){
        ArrayList<String> nonDupList = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        List<Employe> employeList= Employe.find("start is ? and isrepartie is ?",start,true).fetch();
        for (Employe employe: employeList){
            sb.append(employe.getNom().subSequence(0,1)+"."+employe.getPrenom().subSequence(0,1));
            sb.append("/");
        }
       // return sb.substring(0,sb.length()-1);
        if (sb.length()>1){
            return sb.substring(0,sb.length()-1);
        }else {
            return sb.toString();
        }
    }


       public static List<String> getEmployeStart(){
        ArrayList<String> startsList = new ArrayList<String>();
        List<Employe> employeList =Employe.find("isrepartie is ?",true).fetch();
        for (Employe em: employeList){
            startsList.add(em.getStart());
        }
         //  startsList = (ArrayList<String>) JPA.em().createQuery("SELECT i.start FROM Instrument i").getResultList();
      return startsList;
    }
    public static Set mergeTwoList( List<String> l1,List<String> l2){
        ArrayList<String> dupList = new ArrayList<String>();
        dupList.addAll(l1);
        dupList.addAll(l2);
        Set nonDupList = new HashSet(dupList);
        return nonDupList;
    }


    public static void index() {
        render();
    }

    public static void edit(Long id) {
        Employe model = Employe.findById(id);
        notFoundIfNull(model);
        render(model);

    }

    public static void delete(Long id) {
        Employe model = Client.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }

    public static void addVacance(Long id,String nombre,String date_debut,String date_fin) {
        Employe model = Employe.findById(id);
        Vacance vacance1 = new Vacance(model, nombre,date_debut, date_fin);
        notFoundIfNull(model);
        if (vacance1.validateAndSave()) {
            flash.put("success", Messages.get("model.update.success"));
            vacance(model.id);
        }
        flash.put("error", Messages.get("model.update.error"));
        vacance(model.id);
    }
    public static void addSalaire(Long id,double salaire,String date_salaire) {
        Employe model = Employe.findById(id);
        notFoundIfNull(model);
        EmployeSalaire employeSalaire = new EmployeSalaire(model,salaire,date_salaire);
        if (employeSalaire.validateAndSave()) {
            model.setSalaire(salaire);
            model.save();
            flash.put("success", Messages.get("model.update.success"));
            salaire(model.id);
        }
        flash.put("error", Messages.get("model.update.error"));
        salaire(model.id);
    }

    public static void vacance(Long id) {
        Employe model = Employe.findById(id);
        notFoundIfNull(model);
        render(model);
    }
    public static void salaire(Long id) {
        Employe model = Employe.findById(id);
        notFoundIfNull(model);
        render(model);
    }

    public static void update(Employe model) {
        checkAuthenticity();
        notFoundIfNull(model);
        model.setFullName();
        if (model.validateAndSave()) {
            EmployeSalaire employeSalaires = EmployeSalaire.find("employe is ?",model).first();
            if (employeSalaires == null ){
                EmployeSalaire employeSalaire = new EmployeSalaire(model,model.getSalaire(),model.getDate_embauche());
                employeSalaire.save();
            }else{}
            flash.put("success", Messages.get("model.update.success"));
            index();
        }
        flash.put("error", Messages.get("model.update.error"));
        index();

    }
    public static void deleteSalaire(Long id) {
        EmployeSalaire model = EmployeSalaire.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }
    public static void deleteVacance(Long id) {
        Vacance model = Vacance.findById(id);
        notFoundIfNull(model);
        try {
            model.delete();
            flash.put("success", Messages.get("model.delete.success"));
        } catch (Exception e) {
            flash.put("error", Messages.get("model.delete.error"));
        }
    }


}
