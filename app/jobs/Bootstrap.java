package jobs;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import models.*;
import models.System;
import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

import java.util.HashMap;
import java.util.Map;

@OnApplicationStart
public class Bootstrap extends Job {

    @Override
    public void doJob() {

        Logger.info("Application started");
        if (Sexe.count() == 0) {
            Fixtures.loadModels("sexe.yml");
        }
        if (System.count() == 0) {
            Fixtures.loadModels("system.yml");
        }
        if (Province.count() == 0) {
            Fixtures.loadModels("province.yml");
        }
        if (Pays.count() == 0) {
            Fixtures.loadModels("pays.yml");
        }
        if (Disponibilite.count() == 0) {
            Fixtures.loadModels("disponible.yml");
        }
        if (Actif.count() == 0) {
            Fixtures.loadModels("actif.yml");
        }
//        if (StatutTravail.count() == 0) {
//            Fixtures.loadModels("StatutTravail.yml");
//        }

        if (Autorisation.count() == 0) {
            Fixtures.loadModels("autorisations.yml");
        }

        if (TypeInstrument.count() == 0) {
            Fixtures.loadModels("typeInstrument.yml");

            if (Fonction.count() == 0) {
                Fixtures.loadModels("fonction.yml");
            }
            if (User.count() == 0) {
                User user = new User("Michel.mel", "Michel", "Mel", "michel@gmail.com", true);
                user.setDisplayName();
                user.setPassword("passe");
                user.save();
            }
        }

        if (TypeTravail.count() == 0) {
            Fixtures.loadModels("typeTravail.yml");
        }
    }
}
