package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class TypeInstrument extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @JsonProperty
    private String color;

    @JsonProperty
    private String abrev;

    @OneToMany(mappedBy="typeInstrument", cascade = CascadeType.ALL)
    private List<Instrument> instruments;

    public List<Instrument> getInstruments() {
        return instruments;
    }

    public void setInstruments(List<Instrument> instruments) {
        this.instruments = instruments;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAbrev() {
        return abrev;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }


}
