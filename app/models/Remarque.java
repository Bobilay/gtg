package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Remarque extends BaseModel {

    @ManyToOne
    @JsonProperty
    private Instrument instrument;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String remarque;

    @JsonProperty
    private String date_remarque;

    @JsonProperty
    private int note;

    public Remarque(Instrument instrument, String remarque, String date_remarque) {
        this.instrument = instrument;
        this.remarque = remarque;
        this.date_remarque = date_remarque;
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }
}
