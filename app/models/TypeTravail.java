package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class TypeTravail extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @ManyToOne
    @JsonProperty
    private System system;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public static List<TypeTravail> findBySystem(int code){
        return TypeTravail.find("system.code is ?", code).fetch();
    }
}
