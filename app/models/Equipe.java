package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.mindrot.jbcrypt.BCrypt;
import play.Play;
import play.data.binding.NoBinding;
import play.libs.Crypto;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by billyaymeric on 12/09/2016.
 */
@Entity
public class Equipe extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String nom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private int numero;

    @JsonProperty
    private String description;

    @JsonProperty
    private String code;

    @ManyToMany(mappedBy = "equipes", targetEntity = Employe.class)
    @JsonProperty
    private List<Employe> employes;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
