package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Formation extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String detail;

    @JsonProperty
    private Fournisseur fournisseur;

    @JsonProperty
    private Date date_formation;

    @ManyToMany(mappedBy = "formations", targetEntity = Employe.class)
    @JsonProperty
    private List<Employe> employes;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Date getDate_formation() {
        return date_formation;
    }

    public void setDate_formation(Date date_formation) {
        this.date_formation = date_formation;
    }
}
