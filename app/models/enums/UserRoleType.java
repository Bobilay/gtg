package models.enums;

import play.i18n.Messages;

public enum UserRoleType {

    CUSTOMER (500),
    ADMIN (1000);

    private int code;

    UserRoleType(int code) {
        this.code = code;
    }
    
    public int getCode() {
        return this.code;
    }

    public static UserRoleType parseCode(int code) {
        UserRoleType userRole = null;

        for (UserRoleType item : UserRoleType.values()) {
            if (item.getCode() == code) {
                userRole = item;
                break;
            }
        }
        return userRole;
    }

    @Override
    public String toString() {
        return Messages.get(this.name());
    }

}
