package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Dossier extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String code;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
