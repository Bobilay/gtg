package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Instrument extends BaseModel {

    private boolean isrepartie;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String start;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String version;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @JsonProperty
    @ManyToOne
    private TypeInstrument typeInstrument;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String numero;

    @OneToMany(mappedBy="instrument", cascade = CascadeType.ALL)
    private List<Remarque> remarques;

    @OneToMany(mappedBy="instrument", cascade = CascadeType.ALL)
    private List<Entretien> entretiens;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String num_serie;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String model;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String couleur;

    @JsonProperty
    private double cout_aquis;

    @JsonProperty
    private String date_aquis;

    @JsonProperty
    private String fin_garantie;

    @JsonProperty
    @ManyToOne
    private Marque marque;


    @ManyToOne
    @JsonProperty
    private Actif actif;

    @ManyToOne
    @JsonProperty
    private Disponibilite disponible;

    @JsonProperty
    private String compatible;

    @JsonProperty
    @ManyToOne
    private Fournisseur fournisseur;

    @JsonProperty
    private int etat;

    public boolean isIsrepartie() {
        return isrepartie;
    }

    public void setIsrepartie(boolean isrepartie) {
        this.isrepartie = isrepartie;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Actif getActif() {
        return actif;
    }

    public void setActif(Actif actif) {
        this.actif = actif;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCompatible() {
        return compatible;
    }

    public void setCompatible(String compatible) {
        this.compatible = compatible;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public double getCout_aquis() {
        return cout_aquis;
    }

    public void setCout_aquis(double cout_aquis) {
        this.cout_aquis = cout_aquis;
    }

    public String getDate_aquis() {
        return date_aquis;
    }

    public void setDate_aquis(String date_aquis) {
        this.date_aquis = date_aquis;
    }

    public String getFin_garantie() {
        return fin_garantie;
    }

    public void setFin_garantie(String fin_garantie) {
        this.fin_garantie = fin_garantie;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public Disponibilite getDisponible() {
        return disponible;
    }

    public void setDisponible(Disponibilite disponible) {
        this.disponible = disponible;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNum_serie() {
        return num_serie;
    }

    public void setNum_serie(String num_serie) {
        this.num_serie = num_serie;
    }

    public TypeInstrument getTypeInstrument() {
        return typeInstrument;
    }

    public void setTypeInstrument(TypeInstrument typeInstrument) {
        this.typeInstrument = typeInstrument;
    }

    public List<Remarque> getRemarques() {
        return remarques;
    }

    public void setRemarques(List<Remarque> remarques) {
        this.remarques = remarques;
    }

    public List<Entretien> getEntretiens() {
        return entretiens;
    }

    public void setEntretiens(List<Entretien> entretiens) {
        this.entretiens = entretiens;
    }

    public static Instrument findById(long id) {
        return Instrument.find("id is ?", id).first();
    }

}
