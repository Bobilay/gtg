package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class EmployeSalaire extends BaseModel {


    @ManyToOne
    @JsonProperty
    private Employe employe;

    @JsonProperty
    private String date_new_salaire;

    @JsonProperty
    private double salaire;

    public EmployeSalaire(Employe model, double salaire, String date_salaire) {
        this.employe = model;
        this.salaire = salaire;
        this.date_new_salaire = date_salaire;

    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }
}
