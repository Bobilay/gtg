package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class StatutTravail extends BaseModel {

    @JsonProperty
    private int code;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @ManyToOne
    @JsonProperty
    private System system;

    @OneToMany(mappedBy="statutTravail", cascade = CascadeType.ALL)
    private List<Travail> travails;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Travail> getTravails() {
        return travails;
    }

    public void setTravails(List<Travail> travails) {
        this.travails = travails;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
    public static StatutTravail findByCode(int code) {
        return StatutTravail.find("code is ?", code).first();

    }
}
