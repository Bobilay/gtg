package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class System extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String libelle;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @JsonProperty
    private int code;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static System findByCode(String code)
            throws IllegalArgumentException {
        if (code == null) {
            throw new IllegalArgumentException("code parameter cannot be null.");
        } else {
            return System.find("code is ?", code).first();
        }
    }
}
