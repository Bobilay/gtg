package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.mindrot.jbcrypt.BCrypt;
import play.Play;
import play.data.binding.NoBinding;
import play.libs.Crypto;

import javax.persistence.*;
import java.util.*;

/**
 * Created by billyaymeric on 12/09/2016.
 */
@Entity
public class Employe extends BaseModel {

    private boolean isrepartie;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String start;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String prenom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String nom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String fullName;

    @ManyToOne
    @JsonProperty
    private Sexe sexe;

    @JsonProperty
    private String date_naissance;

    @JsonProperty
    private String date_embauche;

    @JsonProperty
    private String telephone;

    @JsonProperty
    private String cellulaire;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String email;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String adresse;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String numero_rue;

    public boolean isIsrepartie() {
        return isrepartie;
    }

    public void setIsrepartie(boolean isrepartie) {
        this.isrepartie = isrepartie;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String code_postal;

    @JsonProperty
    private String code;

    @NoBinding
    private String passwordHash;

    @JsonProperty
    private int status;

    @ManyToOne
    @JsonProperty
    private Diplome diplome;

    @ManyToOne
    @JsonProperty
    private Fonction fonction;

    @ManyToOne
    @JsonProperty
    private System system;

    @ManyToOne
    @JsonProperty
    private Ville ville;

    @JsonProperty
    private double salaire;

    @ManyToOne
    @JsonProperty
    private Pays pays;

    @ManyToMany(targetEntity = Equipe.class)
    @JsonProperty
    private List<Equipe> equipes;

    @ManyToMany(targetEntity = Formation.class)
    @JsonProperty
    private List<Formation> formations;

    @OneToMany(mappedBy="employe", cascade = CascadeType.ALL)
    private List<Vacance> vacances;

    @OneToMany(mappedBy="employe", cascade = CascadeType.ALL)
    private List<EmployeSalaire> salaires;

    public List<Vacance> getVacances() {
        return vacances;
    }

    public void setVacances(List<Vacance> vacances) {
        this.vacances = vacances;
    }

    public List<EmployeSalaire> getSalaires() {
        return salaires;
    }

    public void setSalaires(List<EmployeSalaire> salaires) {
        this.salaires = salaires;
    }

    public Employe() {

    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName() {
        this.fullName = this.prenom + " " + this.nom;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getDate_embauche() {
        return date_embauche;
    }

    public void setDate_embauche(String date_embauche) {
        this.date_embauche = date_embauche;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCellulaire() {
        return cellulaire;
    }

    public void setCellulaire(String cellulaire) {
        this.cellulaire = cellulaire;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Diplome getDiplome() {
        return diplome;
    }

    public void setDiplome(Diplome diplome) {
        this.diplome = diplome;
    }

    public Fonction getFonction() {
        return fonction;
    }

    public void setFonction(Fonction fonction) {
        this.fonction = fonction;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public String getNumero_rue() {
        return numero_rue;
    }

    public void setNumero_rue(String numero_rue) {
        this.numero_rue = numero_rue;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public void setPassword(String password) throws IllegalArgumentException {
        if ((password == null) || (password.length() < 2)) {
            throw new IllegalArgumentException(
                    "password length has to be greater than 8 characters.");
        } else {
            this.passwordHash = Employe.doStrongPasswordHash(password);
        }
    }

    @Override
    public String toString() {
        return this.code;
    }

    public static Employe authenticate(String code, String password) {
        try {
            if ((code == null) || (password == null)) {
                throw new IllegalArgumentException();
            }
            Employe employe = Employe.find("code is ?", code).first();
            if (employe != null) {
                if (BCrypt.checkpw(Employe.doWeakPasswordHash(password), employe.passwordHash)) {
                    return employe;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String doStrongPasswordHash(String password) {
        return BCrypt.hashpw(Employe.doWeakPasswordHash(password), BCrypt.gensalt());
    }

    private static String doWeakPasswordHash(String password) {
        return Crypto.passwordHash(password + Play.configuration.getProperty("application.secret"), Crypto.HashType.SHA512);
    }

    public static Employe findByCode(String code)
            throws IllegalArgumentException {
        if (code == null) {
            throw new IllegalArgumentException("code parameter cannot be null.");
        } else {
            return Employe.find("code is ?", code).first();
        }
    }

    public static List<Employe> findByType(System system)
        throws IllegalArgumentException {
            if (system == null) {
                throw new IllegalArgumentException("code parameter cannot be null.");
            } else {
                return Employe.find("system is ?", system).fetch();
            }
    }

    public static List<Employe> equipes(String fonctionCode, boolean isrepartie){
        return Employe.find("system.code is ? and fonction.code is ? and isrepartie is ? and status is ?",
                1, fonctionCode, isrepartie, 1).fetch();
    }


}
