package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.OneToMany;
import java.util.List;

import javax.persistence.ManyToOne;

/**
 * Created by billyaymeric on 12/09/2016.
 */
@Entity
public class Fournisseur extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String prenom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String nom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String displayName;

    @JsonProperty
    private String contact;

    private String telephone;


    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String email;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String numero_rue;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String code_postal;

    @ManyToOne
    @JsonProperty
    private Ville ville;

    @OneToMany(mappedBy="fournisseur", cascade = CascadeType.ALL)
    private List<Instrument> instruments;

    @OneToMany(mappedBy="fournisseur", cascade = CascadeType.ALL)
    private List<Entretien> entretiens;


    @ManyToOne
    @JsonProperty
    private Pays pays;

    @JsonProperty
    private int status;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName() {
        this.displayName = this.nom+" "+this.prenom;
    }


    public List<Instrument> getInstruments() {
        return instruments;
    }

    public void setInstruments(List<Instrument> instruments) {
        this.instruments = instruments;
    }

    public Fournisseur() {

    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumero_rue() {
        return numero_rue;
    }

    public void setNumero_rue(String numero_rue) {
        this.numero_rue = numero_rue;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public static Fournisseur findByCode(String code)
            throws IllegalArgumentException {
        if (code == null) {
            throw new IllegalArgumentException("code parameter cannot be null.");
        } else {
            return Fournisseur.find("code is ?", code).first();
        }
    }



}
