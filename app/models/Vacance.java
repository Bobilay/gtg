package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Vacance extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String nombre;

    @JsonProperty
    private String date_debut;

    @JsonProperty
    private String date_fin;

    @ManyToOne
    @JsonProperty
    private Employe employe;

    public Vacance(Employe model, String nombre, String date_debut, String date_fin) {
        this.employe=model;
        this.date_debut=date_debut;
        this.nombre=nombre;
        this.date_fin=date_fin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }


    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }
}
