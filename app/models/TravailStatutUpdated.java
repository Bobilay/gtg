package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class TravailStatutUpdated extends BaseModel {

    @JsonProperty
    private Travail travail;

    @JsonProperty
    private StatutTravail statutTravail;

    public Travail getTravail() {
        return travail;
    }

    public void setTravail(Travail travail) {
        this.travail = travail;
    }

    public StatutTravail getStatutTravail() {
        return statutTravail;
    }

    public void setStatutTravail(StatutTravail statutTravail) {
        this.statutTravail = statutTravail;
    }
}
