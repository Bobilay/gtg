package models;


import models.deadbolt.Role;
import models.enums.UserRoleType;

public class UserRole implements Role {

    private int code;

    public UserRole(int code) {
        this.code = code;
    }

    @Override
    public String getRoleName() {
        return UserRoleType.parseCode(this.code).name();
    }

    public final static String ADMIN = "ADMIN";
    public final static String CUSTOMER = "CUSTOMER";
    public final static String [] ROLES = {ADMIN, CUSTOMER};

}
