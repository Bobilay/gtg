package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Entretien extends BaseModel {

    @JsonProperty
    private double cout;

    @JsonProperty
    private String date_entretien;

    @JsonProperty
    @ManyToOne
    private Fournisseur fournisseur;

    @ManyToOne
    @JsonProperty
    private Instrument instrument;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String detail;

    public Entretien(Instrument model, double cout, String date_entretien, String detail, Fournisseur frs) {
        this.instrument=model;
        this.cout=cout;
        this.detail=detail;
        this.fournisseur=frs;
        this.date_entretien=date_entretien;
    }

    public double getCout() {
        return cout;
    }

    public void setCout(double cout) {
        this.cout = cout;
    }

    public String getDate_entretien() {
        return date_entretien;
    }

    public void setDate_entretien(String date_entretien) {
        this.date_entretien = date_entretien;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
