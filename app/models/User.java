package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import models.deadbolt.RoleHolder;
import models.enums.UserRoleType;
import org.hibernate.annotations.Type;
import org.mindrot.jbcrypt.BCrypt;
import play.Play;
import play.data.binding.NoBinding;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.libs.Crypto;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Entity(name = "user_")
public class User extends BaseModel implements RoleHolder {

    final public static String SESSION_USERNAME_KEY = "username";
    final public static String SESSION_FULLNAME_KEY = "fullname";
    final public static String SESSION_AUTORISATION_KEY = "autorisations";

    final public static String VIEW_VARIABLE_NAME = "user";

    @Unique
    public String login;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String prenom;

    @Type(type = "org.hibernate.type.TextType")
    @JsonProperty
    protected String displayName;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String nom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String code_user;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String contact;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String email;

    @ManyToOne
    @JsonProperty
    private Fonction fonction;

    @JsonProperty
    private boolean actif;


    public int roleCode;

    @ManyToMany(targetEntity = Autorisation.class)
    @JsonProperty
    private List<Autorisation> autorisations;

    @NoBinding
    @Required
    private String passwordHash;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName() {
        this.displayName = this.nom+" "+this.prenom;
    }


    public String getCode_user() {
        return code_user;
    }

    public void setCode_user(String code_user) {
        this.code_user = code_user;
    }

    public List<Autorisation> getAutorisations() {
        return autorisations;
    }

    public void setAutorisations(List<Autorisation> autorisations) {
        this.autorisations = autorisations;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Fonction getFonction() {
        return fonction;
    }

    public void setFonction(Fonction fonction) {
        this.fonction = fonction;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public void setPassword(String password) throws IllegalArgumentException {
        if ((password == null) || (password.length() < 4)) {
            throw new IllegalArgumentException(
                    "password length has to be greater than 8 characters.");
        } else {
            this.passwordHash = User.doStrongPasswordHash(password);
        }
    }

    @Override
    public String toString() {
        return this.login;
    }

    public static User authenticate(String login, String password) {
        if ((login == null) || (password == null)) {
            throw new IllegalArgumentException();
        }
        User user = User.find("login is ?", login).first();
        if (user != null) {
            if (BCrypt.checkpw(User.doWeakPasswordHash(password), user.passwordHash)) {
                return user;
            }
        }
        return null;
    }

    private static String doStrongPasswordHash(String password) {
        return BCrypt.hashpw(User.doWeakPasswordHash(password), BCrypt.gensalt());
    }

    private static String doWeakPasswordHash(String password) {
        return Crypto.passwordHash(password + Play.configuration.getProperty("application.secret"), Crypto.HashType.SHA512);
    }

    public static User findByLogin(String login)
            throws IllegalArgumentException {
        if (login == null) {
            throw new IllegalArgumentException("login parameter cannot be null.");
        } else {
            return User.find("login is ?", login).first();
        }
    }

    public int getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(int roleCode) {
        this.roleCode = roleCode;
    }

    public UserRoleType getRoleType() {
        return UserRoleType.parseCode(this.roleCode);
    }

    @Override
    public List<UserRole> getRoles() {
        List<UserRole> roles = new ArrayList<UserRole>();
        roles.add(new UserRole(this.roleCode));
        return roles;
    }

    public User(String login, String prenom, String nom, String email, boolean actif) {
        this.login = login;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.actif = actif;
    }
}
