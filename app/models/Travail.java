package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.System;
import java.text.ParseException;

/**
 * Created by bobilay on 11/05/2017.
 */
@Entity
public class Travail extends BaseModel {

    private boolean isrepartie;

    private boolean dead;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String start;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String numero;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String titre;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String instrumentation;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String doc;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String lieu;


    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String beneficiaire;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String duree;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String deadLine;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String rdv; //date de rendez-vous

    @JsonProperty
    private String priorite;

    @ManyToOne
    @JsonProperty
    private TypeTravail typeTravail;

    @ManyToOne
    @JsonProperty
    private TypeMandat typeMandat;

    @JsonProperty
    @ManyToOne
    private StatutTravail statutTravail;

    @ManyToOne
    @JsonProperty
    private Bureau bureau;

    @ManyToOne
    @JsonProperty
    private Dossier dossier;

    @ManyToOne
    @JsonProperty
    private Client client;

    @ManyToOne
    @JsonProperty
    private User user;

    @ManyToOne
    @JsonProperty
    private Employe employe;


    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getBeneficiaire() {
        return beneficiaire;
    }

    public void setBeneficiaire(String beneficiaire) {
        this.beneficiaire = beneficiaire;
    }

    public boolean isIsrepartie() {
        return isrepartie;
    }

    public void setIsrepartie(boolean isrepartie) {
        this.isrepartie = isrepartie;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getInstrumentation() {
        return instrumentation;
    }

    public void setInstrumentation(String instrumentation) {
        this.instrumentation = instrumentation;
    }

    public String getRdv() {
        return rdv;
    }

    public void setRdv(String rdv) {
        this.rdv = rdv;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public StatutTravail getStatutTravail() {
        return statutTravail;
    }

    public void setStatutTravail(StatutTravail statutTravail) {
        this.statutTravail = statutTravail;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(String deadLine) {
        this.deadLine = deadLine;
    }

    public String getPriorite() {
        return priorite;
    }

    public void setPriorite(String priorite) {
        this.priorite = priorite;
    }

    public TypeTravail getTypeTravail() {
        return typeTravail;
    }

    public void setTypeTravail(TypeTravail typeTravail) {
        this.typeTravail = typeTravail;
    }

    public TypeMandat getTypeMandat() {
        return typeMandat;
    }

    public void setTypeMandat(TypeMandat typeMandat) {
        this.typeMandat = typeMandat;
    }

    public Bureau getBureau() {
        return bureau;
    }

    public void setBureau(Bureau bureau) {
        this.bureau = bureau;
    }

    public Dossier getDossier() {
        return dossier;
    }

    public void setDossier(Dossier dossier) {
        this.dossier = dossier;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public static List<Travail> findBySystem(int code) {
        return Travail.find("typeTravail.system.code is ?", code).fetch();
    }

    public static Travail findById(long id) {
        return Travail.find("id is ?", id).first();
    }

    public void setDead(String deadLine) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        try {

            Date date = formatter.parse(deadLine);
            System.out.println(date);
            System.out.println((new Date()).compareTo(date));
            int comp = (new Date()).compareTo(date);
            this.dead = comp > 0;

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}