package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.mindrot.jbcrypt.BCrypt;
import play.Play;
import play.data.binding.NoBinding;
import play.libs.Crypto;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by billyaymeric on 12/09/2016.
 */
@Entity
public class Client extends BaseModel {

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String prenom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String nom;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String fullName;

    @JsonProperty
    private String telephone;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String email;

    @ManyToOne
    @JsonProperty
    private Ville ville;

    @ManyToOne
    @JsonProperty
    private Pays pays;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String code_postal;

    @JsonProperty
    @Type(type = "org.hibernate.type.TextType")
    private String numero_rue;

    @JsonProperty
    private int status;


    public Client() {

    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName() {
        this.fullName = this.nom+" "+this.prenom;;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public String getNumero_rue() {
        return numero_rue;
    }

    public void setNumero_rue(String numero_rue) {
        this.numero_rue = numero_rue;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public static Client findByCode(String code)
            throws IllegalArgumentException {
        if (code == null) {
            throw new IllegalArgumentException("code parameter cannot be null.");
        } else {
            return Client.find("code is ?", code).first();
        }
    }



}
