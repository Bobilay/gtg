$('#model_category_id').change(function() {
    var tvaler = $( "#model_category_id option:selected" ).text();
    var n = (tvaler.toUpperCase().trim()).localeCompare("stock".toUpperCase().trim());
    var i = (tvaler.toUpperCase().trim()).localeCompare("cloture".toUpperCase().trim());
    if (n==0 || i==0){
        $( "#typeDeReponse").css('display', 'none');
        $( "#items").css('display', 'none');
    }else {
        $( "#typeDeReponse").css('display', 'block');
    }
    if (n==0){
        $( "#typeDeQuestion").css('display', 'none');
    }else {
        $( "#typeDeQuestion").css('display', 'block');
    }
});

$('.faq :submit').click(function() {
    var base_url ="/protic";
    //var base_url =url;
    $('.btn-flat').attr('type', 'button');

    var id = $("input[name='model.id']").val();
    var debut = $("input[name='model.debut']").val();
    var pas = $("input[name='model.pas']").val();
    var fin = $("input[name='model.fin']").val();
    var categorie = $( "#model_category_id option:selected" ).text();
    var categorieId = $( "#model_category_id option:selected" ).val();
    var typeClient = $( "#model_typeclientfaq_id option:selected" ).text();
    var typeClientId = $( "#model_typeclientfaq_id option:selected" ).val();
    var typeReponse = $( "#model_typeReponse_id option:selected" ).text();
    var typeReponseId = $( "#model_typeReponse_id option:selected" ).val();
    var libelle = $( "#model_libelle option:selected" ).text();
    var libelleId = $( "#model_libelle option:selected" ).val();
    var question = $('textarea#model_question').val();

    console.log("id: "+id);
    console.log("debut: "+debut);
    console.log("pas: "+pas);
    console.log("fin: "+fin);
    console.log("categorie: "+categorie);
    console.log("categorieId: "+categorieId);
    console.log("typeClient: "+typeClient);
    console.log("typeClientId: "+typeClientId);
    console.log("typereponse: "+typeReponse);
    console.log("typereponseId: "+typeReponseId);
    console.log("libelleFaq: "+libelle);
    console.log("libelleFaqId: "+libelleId);
    console.log("question: "+question);
    console.log("---------------------------------------------------------------");
   $.ajax({

       // url     :base_url+"/admin/send/faq",
        url     :"/admin/send/faq",
        type    : 'post',
        data    :  {
            id                : id,
            typeClient        : typeClient,
            categorie         : categorie,
            typeReponse       : typeReponse,
            libelle           : libelle,
            question          : question,
            debut             : parseInt(debut),
            pas               : parseInt(pas),
            fin               : parseInt(fin)
        },
        success : function() {
            window.location.replace("/admin/faq");
           // window.location.replace(base_url+"/admin/faq");
        }
    });

});
$('#model_libelle').change(function() {
    var valer = $( "#model_libelle option:selected" ).text();
    var n = (valer.toUpperCase().trim()).localeCompare("optionnel".toUpperCase().trim());
    var i = (valer.toUpperCase().trim()).localeCompare("multiple".toUpperCase().trim());
    if (n==0 || i==0){
        $( "#items").css('display', 'block');
    }else {
        $( "#items").css('display', 'none');
    }
});



