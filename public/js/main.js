var userAjaxHandler = {
  delete: function (id, url) {
   // var base_url ='/protic'+url;
    var base_url =url;
    console.log(base_url);
    $.ajax({
      url: base_url + id,
        type: "DELETE",
        data: {
        id: id
      }, success: function () {
        location.reload();
      }
    })
  }
};

var mapHandler = {
  show: function () {

  }
};

var _toastrStateConfig = function () {
  toastr.options.closeButton = false;
  toastr.options.progressBar = false;
  toastr.options.debug = false;
  toastr.options.positionClass = 'toast-bottom-left';
  toastr.options.showDuration = 333;
  toastr.options.hideDuration = 333;
  toastr.options.timeOut = 4000;
  toastr.options.extendedTimeOut = 4000;
  toastr.options.showEasing = 'swing';
  toastr.options.hideEasing = 'swing';
  toastr.options.showMethod = 'slideDown';
  toastr.options.hideMethod = 'slideUp';
};

var showSuccess = function (message) {
  toastr.clear();

  this._toastrStateConfig();
  toastr.options.closeButton = true;
  toastr.info(message, 'Information Opération');
};

var showError = function (message) {
  toastr.clear();

  this._toastrStateConfig();
  toastr.options.closeButton = true;
  toastr.error(message, 'Erreur Opération');
};